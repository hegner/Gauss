// $Id: lcg_selection.h,v 1.1 2009-02-11 08:18:06 cattanem Exp $
#ifndef GENEVENT_LCGDICT_H
#define GENEVENT_LCGDICT_H 1

// Additional classes to be added to automatically generated lcgdict

// begin include files
#include "HepMC/GenVertex.h"
#include "Event/GenHeader.h"
#include "Event/ProcessHeader.h"
#include "Event/BeamParameters.h"
#include "Event/GenCollision.h"
#include "Event/ProcessHeader.h"
#include "Event/HepMCEvent.h"
// end include files
#include <vector>

namespace {
  struct _Instantiations {
    // begin instantiations
    LHCb::GenHeader genHeader;
    LHCb::ProcessHeader procHeader;
    LHCb::BeamParameters beamParams;
    LHCb::GenCollision genColl;
    LHCb::GenCollisions genCollColl;
    LHCb::HepMCEvent hepMCEvent;
    SmartRef<LHCb::HepMCEvent*> hepMCEventVec;
    std::vector<LHCb::HepMCEvent*> hepMCEventRef;
    LHCb::HepMCEvents hepMCEventColl;
    SmartRef<LHCb::GenHeader> genHeaderRef;
    SmartRef<LHCb::BeamParameters> beamParamsRef;
    SmartRefVector<LHCb::GenCollision> genCollVecRef;
    SmartRef<LHCb::GenCollision> genCollRef;
    std::vector<SmartRef<LHCb::GenCollision> > genCollVec;
    std::vector<LHCb::GenCollision*> genCollVecPtr;
    // end instantiations
  };
}

#endif // GENEVENT_LCGDICT_H
