#ifndef GENPYTHIA8_ILHAUPFORTRANTOOL_H 
#define GENPYTHIA8_ILHAUPFORTRANTOOL_H 1

// Include files
// from Gaudi
#include "GaudiKernel/IAlgTool.h"

class GenLHAupFortran ;

static const InterfaceID IID_ILHAupFortranTool ( "ILHAupFortranTool", 1, 0 );

/** @class ILHAupFortranTool ILHAupFortranTool.h GenPythia8/ILHAupFortranTool.h
 *  
 *  Interface for tools used to interface with Pythia Fortran User Processes
 *
 *  @author Patrick Robbe
 *  @date   2012-11-30
 */
class ILHAupFortranTool : virtual public IAlgTool {
public: 

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_ILHAupFortranTool; }

  virtual GenLHAupFortran * getLHAupPtr( ) = 0 ;
  
};
#endif // GENPYTHIA8_ILHAUPFORTRANTOOL_H
