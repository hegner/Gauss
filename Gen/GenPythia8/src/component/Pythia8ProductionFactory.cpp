// Gaudi.
#include "GaudiKernel/ToolFactory.h"

// GenPythia8.
#include "GenPythia8/Pythia8Production.h"

// Declare the Pythia8Production tool.
DECLARE_TOOL_FACTORY(Pythia8Production)

